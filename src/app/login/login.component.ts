
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../module/user.model';
import { LoginService } from '../service/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new User();
  erreur=0;

  constructor(private loginService : LoginService,
    private router: Router) { }
    

  ngOnInit(): void {
  } 
  onLoggedin()
  {
    this.loginService.getUserFromDB(this.user.email).subscribe((usr:User) => {
    if (usr.password==this.user.password)
    {
      this.loginService.signIn(usr);
      this.router.navigate(['/']);
      }
      else
      this.erreur = 1;
      },(err) => console.log(err));
    
  

}
}
