import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const AUTH_API = 'http://localhost:8080/login';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class registerService {

  constructor(private http: HttpClient) { }

  register(credentials: { email: any; password: any; }): Observable<any> {
    return this.http.post(AUTH_API + 'register', {
      email: credentials.email,
      password: credentials.password
    }, httpOptions);
  }}


