import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Role } from '../module/role.model';
import { User } from '../module/user.model';



@Injectable({
  providedIn: 'root'
})
export class LoginService {
 /* users: User[] = [{"email":"admin","password":"123","roles":['ADMIN']},
  {"email":"amin","password":"123","roles":['USER']} ]*/

  apiURL: string = 'http://localhost:8080/login';
  public loggedUser:string | undefined;
public isloggedIn: Boolean = false;
public roles!:Role[]| undefined;

constructor(private route:Router,private Http:HttpClient){}

getUserFromDB(email:string):Observable<User>
{
const url = `${this.apiURL}/${email}`;
return this.Http.get<User>(url);
}
signIn(user :User){
  this.loggedUser = user.email;
  this.isloggedIn = true;
  this.roles = user.roles;
  localStorage.setItem('loggedUser',this.loggedUser);
  localStorage.setItem('isloggedIn',String(this.isloggedIn));
  }



isAdmin():Boolean{
  if (!this.roles) //this.roles== undefiened
  return false;
  return (this.roles.indexOf('ADMIN') >-1) ;
  ;
  }
  
Logout() {
  this.isloggedIn= false;
  this.loggedUser=undefined;
  this.roles = undefined;
  localStorage.removeItem('loggedUser');
  localStorage.setItem('isloggedIn',String(this.isloggedIn));
  this.route.navigate(['/login']);
  
  }
  
}


